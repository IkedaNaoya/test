﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hit_light_red : MonoBehaviour
{

    public GameObject slight;
    float LIntensity = 0f;



    // 当たった時に呼ばれる関数
    void OnTriggerStay(Collider Cylinder)
    {
        Debug.Log("test");
        GetComponent<Renderer>().material.color = Color.red;
        LIntensity = 2f;
        slight.GetComponent<Light>().intensity = LIntensity;

    }

    void OnTriggerExit(Collider Cylinder)
    {
        GetComponent<Renderer>().material.color = Color.white;
        LIntensity = 0f;
        slight.GetComponent<Light>().intensity = LIntensity;
    }


}
